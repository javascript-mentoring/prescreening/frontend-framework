module.exports = {
  transform: {
    "^.+\\.[t|j]sx?$": "babel-jest",
  },
  setupFilesAfterEnv: ["<rootDir>/setup-tests.js"],
  reporters: [ "default", "jest-junit" ],
};
