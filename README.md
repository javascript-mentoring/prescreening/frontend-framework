# Front-End Framework PreScreening

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)
2. Fork this repository
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/frontend-framework-prescreening/  
4. Go to folder `frontend-framework-prescreening`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests

## Submit to [AutoCode](https://autocode.lab.epam.com/)
1. Open [AutoCode](https://autocode.lab.epam.com/) and login
2. Subscribe to Frontend Framework Prescreening course
3. Select your task
4. Submit your solution to Autocode

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
