/**
 * Export a function named "promisify".
 * The function should take a callback-oriented function in arguments
 * and return it's promisified variant.
 * The promisified variant instead of calling a callback should return a promise that
 * resolves or rejects based on the existence of the error.
 * Refer to tests for an example.
 */

export function promisify(fn) {
  
}
