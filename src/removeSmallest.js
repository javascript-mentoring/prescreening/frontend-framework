/**
 * Export a function named "removeSmallest".
 * The function should take an array of numbers in any order.
 * The function should return a copy of input array without the first smallest number.
 * Numbers may repeat, the function shall ignore all occurencies of the smallest number except the first one.
 * Original array should not be mutated.
 * Refer to tests for an example.
 */

export function removeSmallest(numbers) {
  
}
