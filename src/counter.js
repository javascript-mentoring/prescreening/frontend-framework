/**
 * Export a function named createCounter.
 * The function should return an object with a method "count".
 * Calling "count" should return a number that increments on every invocation starting from 0.
 * Multiple instances of counter should be independent and must not share data.
 * Refer to tests for an example.
 */

export function createCounter () {
  
}
