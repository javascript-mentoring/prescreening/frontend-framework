/**
 * Export a function named "run".
 * The function should append the following element tree to the document body:
 * 
 * ```
 * <div>
 *   <button data-testid="like">👍</button>
 *   <button data-testid="dislike">👎</button>
 * </div>
 * ```
 * 
 * Implement behavior based on the requirements below:
 * 
 * Buttons may be active or inactive.
 * When a button is active it has an "active" classname.
 * Only one button can be active at any point of time.
 * Initially both buttons are inactive.
 * 
 * Given none of the buttons are active,
 * when clicking on the "Like" button,
 * it should become active.
 * 
 * Given none of the buttons are active,
 * when clicking on the "Dislike" button,
 * it should become active.
 * 
 * Given the "Like" button is active,
 * when clicking on the "Like" button,
 * it should become inactive.
 * 
 * Given the "Dislike" button is active,
 * when clicking on the "Dislike" button,
 * it should become inactive.
 * 
 * Given the "Like" button is active,
 * when clicking on the "Dislike" button,
 * the "Like" button should become inactive
 * and the "Dislike" button should become active.
 * 
 * Given the "Dislike" button is active,
 * when clicking on the "Like" button,
 * the "Dislike" button should become inactive
 * and the "Like" button should become active.
 */

export function run() {
  
}
