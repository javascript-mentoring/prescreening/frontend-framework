/**
 * Export a function named `getPostWithComments`.
 * The function should take a post ID in argument.
 * The function should make necessary requests to https://jsonplaceholder.typicode.com
 * to get a post by post ID and all comments related to the post.
 * The function should return a promise that resolves with an object of two properties:
 * - "post" — containing the post data
 * - "comments" — containing an array of comments
 * The function should use fetch API to make requests.
 */

export function getPostWithComments(postid) {
  
}
