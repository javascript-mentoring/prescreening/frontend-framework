const { getPostWithComments } = require("../src/api-client");

test("returns a promise with a post and comments", async () => {
  const { post, comments } = await getPostWithComments(1);

  expect(post).toEqual(
    expect.objectContaining({
      id: 1,
    })
  );
  expect(comments.length).toBe(5);
});
