const { removeSmallest } = require("../src/removeSmallest");

describe("removeSmallest", () => {
  test("returns a copy of input array without the first smallest number", () => {
    expect(removeSmallest([5, 1, 2, 4, 1, 3])).toEqual([5, 2, 4, 1, 3]);
    expect(removeSmallest([5, 2, 4, 1, 3])).toEqual([5, 2, 4, 3]);
    expect(removeSmallest([5, 2, 4, 3])).toEqual([5, 4, 3]);
    expect(removeSmallest([5, 4, 3])).toEqual([5, 4]);
    expect(removeSmallest([5, 4])).toEqual([5]);
    expect(removeSmallest([5])).toEqual([]);
    expect(removeSmallest([])).toEqual([]);
  });

  test("does not change input array", () => {
    const input = [6, 2, 4, 1, 3];
    removeSmallest(input);
    expect(input).toEqual([6, 2, 4, 1, 3]);
  });
});
