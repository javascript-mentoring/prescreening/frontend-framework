import { createCounter } from "../src/counter";

describe("Counter", () => {
  test("returns incremented number on each invocation starting with 0", () => {
    const counter = createCounter();

    expect(counter.count()).toBe(0);
    expect(counter.count()).toBe(1);
    expect(counter.count()).toBe(2);
    expect(counter.count()).toBe(3);
  });

  test("several counters don't share state", () => {
    const counter1 = createCounter();
    const counter2 = createCounter();

    expect(counter1.count()).toBe(0);
    expect(counter1.count()).toBe(1);
    expect(counter2.count()).toBe(0);
    expect(counter1.count()).toBe(2);
    expect(counter2.count()).toBe(1);
    expect(counter2.count()).toBe(2);
  });
});
