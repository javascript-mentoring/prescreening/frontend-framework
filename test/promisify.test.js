const { promisify } = require("../src/promisify");

const Emitter = () => {
  const listeners = [];

  return {
    emit: () => listeners.forEach((l) => l()),
    subscribe: (listener) => listeners.push(listener),
  };
};

test("resolved promise", () => {
  expect.assertions(1);
  const emitter = Emitter();
  function fn(value, cb) {
    emitter.subscribe(() => cb(null, value));
  }
  const promisified = promisify(fn);
  const promise = promisified("hello");

  emitter.emit();
  return expect(promise).resolves.toEqual("hello");
});

test("rejected promise", () => {
  expect.assertions(1);
  const emitter = Emitter();
  function fn(err, cb) {
    emitter.subscribe(() => cb(err));
  }
  const promisified = promisify(fn);
  const promise = promisified(new Error("fetch failed"));

  emitter.emit();
  return expect(promise).rejects.toThrow("fetch failed");
});
