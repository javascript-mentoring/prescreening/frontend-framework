import { screen, fireEvent } from "@testing-library/dom";
import { run } from "../src/like";

test("like", () => {
  run();

  expect(screen.getByTestId("like")).not.toHaveClass("active");
  expect(screen.getByTestId("dislike")).not.toHaveClass("active");

  fireEvent.click(screen.getByTestId("like"));
  expect(screen.getByTestId("like")).toHaveClass("active");
  expect(screen.getByTestId("dislike")).not.toHaveClass("active");

  fireEvent.click(screen.getByTestId("dislike"));
  expect(screen.getByTestId("like")).not.toHaveClass("active");
  expect(screen.getByTestId("dislike")).toHaveClass("active");

  fireEvent.click(screen.getByTestId("dislike"));
  expect(screen.getByTestId("like")).not.toHaveClass("active");
  expect(screen.getByTestId("dislike")).not.toHaveClass("active");
});
